---
title: Peer Production License
author:
- Sutty
permalink: licencia/
layout: post
liquid: false
usuaries:
- 1
---



This is a human-readable summary of the [full license](https://wiki.p2pfoundation.net/Peer_Production_License).

## You are free to

**Share** -- copy and redistribute the material in any medium or format

**Adapt** -- remix, transform, and build upon the material

## Under the following terms:

**Attribution** -- You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

**ShareAlike** -- If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

**Non-Capitalist** -- Commercial exploitation of this work is only allowed to cooperatives, non-profit organizations and collectives, worker-owned organizations, and any organization without exploitation relations. Any surplus value obtained by the exercise of the rights given by this work's license must be distributed by and amongst workers.'

## Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.