---
title: Preguntas Frecuentes
description: Respuestas a las Preguntas Frecuentes acerca de Sutty, su uso y tecnología.
author:
- Sutty
image:
  description: ''
categories:
- FAQ
tags:
- Preguntas Frecuentes
- FAQ
order: 13
layout: post
uuid: 750969a7-ae5e-4911-ab3b-54b02d550614
liquid: false
usuaries:
- 5
---

## ¿Cómo crear o mejorar mi página web?

Muches nos preguntamos cómo darle visibilidad a nuestros proyectos más allá de las redes sociales. Para crear blogs o páginas web de forma gratuita, es usual acudir a herramientas como Wordpress, Wix o Drupal. Estos son programas o plataformas de _gestión de contenidos_, que permiten crear, publicar, administrar y mantener sitios web. Pero lo que Drupal o Wordpress no pueden hacer es asegurarnos que nuestras páginas web sean seguras y que aparezcan entre los primeros resultados en los buscadores, porque su propia tecnología tiene algunos puntos débiles...que hasta ahora tal vez nadie te contó.

Si querés crear o actualizar tu página web, ahora existe Sutty, una gestora para sitios web simple pero superadora, que utiliza la técnica de "sitios estáticos" para aumentar la velocidad de carga y la seguridad de tu contenido.

## ¿Qué opciones técnicas existen a nuestro alcance?

Las páginas web se pueden clasificar entre sitios estáticos y sitios dinámicos. Lo "estático" o "dinámico" no tiene nada que ver con la apariencia, estética o interacciones de la página, sino con el proceso de armado, generación o _compilación interna_ del sitio.

## Lo de siempre: sitios dinámicos, tecnología utilizada en Wordpress, Wix, Drupal...

Cuando nuestro navegador descarga una página del sitio web, el sitio dinámico junta distintos recursos (texto, imágenes, diseño, estructura) para generarla y enviarla como respuesta, y lo repite por cada visita, una y otra vez. Todo esto es realizado por el programa (software) que genera las páginas que componen el sitio. Como este programa debe estar disponible para cada visita, si deja de funcionar, el sitio se cae (es decir, ¡no hay sistema!).

Además, un ataque que aproveche alguna vulnerabilidad del programa (de las miles que aman a Wordpress), puede modificarlo para que empiece a generar "páginas maliciosas": páginas creadas por fuera de nuestro control, con publicidad, virus y otras yerbas.

Estas vulnerabilidades obligan a les usuaries- o a quienes éstos hayan derivado la gestión del sitio- a aumentar los esfuerzos en un permanente mantenimiento técnico, constantes actualizaciones e implementación de unas buenas políticas de seguridad específicas solamente para mantener el sitio activo (que no "se caiga"). Son muchos los componentes que deben funcionar de manera constante para que el sitio permanezca accesible. Basta que una parte falle, para que todo se caiga.

En general, estas consideraciones no son tenidas en cuenta bajo la promesa de crear un sitio en forma "rápida y fácil".

## Lo nuevo: sitios estáticos, la tecnología aliada de Sutty

La tecnología de los sitios estáticos funciona distinto a la de los dinámicos. El proceso de generación se hace de una vez. Esto significa que cada vez que el sitio es modificado o actualizado por sus administradorxs, el sitio se "arma" y queda disponible en el servidor como un conjunto de archivos ya listos para ser visitados. Cuando les visitantes acceden a dicho sitio desde Internet, lo ven sin necesidad de que haya ningún programa extra mediando, como en el caso de los sitios dinámicos, que requieren de ese programa que los genere por cada visita. Al no haber programas corriendo por cada visita, no hay riesgos o vulnerabilidades asociados ¡y se elimina el problema potencial de seguridad!

Por eso, en lugar de seguir usando nuestro tiempo corriendo detrás de errores y actualizaciones que podemos ahorrarnos, ¡elegimos los sitios estáticos!

## ¿En serio es más seguro? ¿No hay ninguna vulnerabilidad en los sitios estáticos?

Podemos afirmar que no hay vulnerabilidades que afecten al sitio, como explicábamos recién.

La única posibilidad de ataque es que se ingrese al servidor donde se encuentran alojados y se modifiquen sus archivos. Pero eso es mucho mucho mucho más difícil. Se trata de un tipo de ataque menos común, que requiere de mayor trabajo y en Sutty tomamos medidas de cuidados. Usamos Software Libre nuestros servidores y mantenemos al día las actualizaciones de seguridad. Además, todas las conexiones del servidor son cifradas.

## ¿Qué mejoras puedo obtener programando mi página web como sitio estático en Sutty?

- **Seguridad.** Será más segura sin la necesidad de invertir recursos adicionales para ello, realizar actualizaciones o mantenimientos especiales.
- **Velocidad.** Será más liviano y veloz para navegar, sin que por ello deba verse diferente a otros sitios bonitos.
- **Sustentabilidad.** Ahorrará recursos como hardware y ancho de banda, por lo que seguirá criterios ecológicos.
- **Igualdad de oportunidades/ reducción de la brecha digital.** Al ser más livianos y rápidos, también serán más accesibles para quienes no disponen de dispositivos con grandes recursos.
- **Accesibilidad.** Tendrán automáticamente todas las mejoras introducidas por Sutty para que las tecnologías se adapten a las personas y no las personas a las tecnologías, cuidando de no ser excluyentes para personas con diversidad funcional, neurodiversas o sobrevivientes de traumas.
- **Transportabilidad.** Podrán ser llevados en un dispositivo de almacenamiento básico (como un pendrive), como un único archivo empaquetado para ser compartido y accedido incluso sin necesidad de conectarse a Internet.
- **Distribución.** Copias del sitio podrán ser alojadas en diferentes lugares (servidores), ¡contribuyendo a una red descentralizada y distribuida y garantizando que el contenido pueda estar siempre disponible para compartir!
- **Mejor posicionamiento, más visibilidad.** Crear tu página web con Sutty significa diseñar un sitio liviano, que ocupa poco espacio y, por lo tanto, carga rápido. Esta es una de las condiciones para aparecer en la primera página de los buscadores. Además, Sutty integra otras herramientas específicas de posicionamiento para contribuir con la visibilidad de tu sitio.

## ¿Qué página web puedo crear a través de Sutty?

Cuando registrás una cuenta en Sutty, podés crear y administrar tus sitios (¡sí, puede ser más de uno!) y elegir dónde almacenarlos. Sutty te asiste en todo el proceso de creación, alojamiento y administración de tu sitio, mediante ayudas en la plataforma.

> Adicionalmente, si quisieras algun _feature_ o utilidad que todavía no existiera o algún diseño personalizado, podés ofrecernos que lo hagamos por un intercambio (en dineros, monedas electrónicas o algo que acordemos) o implementarla por tu cuenta.

## Con Sutty, tus páginas web pueden ser:

- más livianas
- más seguras
- ecológicas
- copiadas o compartidas entre compañeres ¡incluso sin conexión a Internet!
- más simples o más complejas en su diseño, según lo que prefieras
- bonitas y maravillosas
- editables y actualizables a través de un panel diseñado para ser simple
- integradas con animaciones, videos, cuentas, chats, formularios, mapas y otros elementos dinámicos
- alojadas en más de un lugar, por más de una persona o máquina, y entre pares
- llevadas como un archivo en un disco externo sin necesidad de disponer de mucho espacio de almacenamiento
- confiables y coherentes con nuestras propuestas de transformación social y ambiental

## A qué NO nos referimos con sitios estáticos en Sutty

**No son sitios:**

- de la Web 1.0, viejos, sin estilo, sin diseño, sin colores o sin onda
- que no puedan modificarse ni actualizarse
- cuyo contenido o diseño sean demasiado complejo de editar o que no pueda hacerse rápidamente
- en los que no haya interacción con sus interlocutores/as
- que no tengan bases de datos
- que no permitan loguear ni gestionar usuaries
- que no admitan foros o chats
- que no admitan formularios de consulta
- que no tengan correos electrónicos asociados
- cuyo contenido no este "personalizado"
- que no pueda levantar y mostrar noticias
- que no puedan interactuar con redes sociales
- que no puedan mostrar contenido multimedia
- que no puedan incorporar recursos para compras o donaciones
- que usen solamente el lenguaje HTML

## ¿Con qué tecnología trabajan? ¿Podemos saber algo más?

**¡Sutty no es solo una gestora de contenidos!** Sutty es una plataforma que permite crear, gestionar e implementar mejoras para sitios web estáticos, con la tecnología de Jekyll.

Jekyll es el programa desarrollado en el lenguaje Ruby que toma las plantillas, artículos y otros recursos de un sitio y genera la colección de archivos que va a ser el sitio publicado. Reemplaza la base de datos y el programa que genera las paginas en tecnologías como Wordpress o Drupal. Así, nos permite evitar bases de datos obligatorias y actualizaciones problemáticas, privilegiando una forma más directa y simple de actualizar contenidos en forma de publicaciones. Usa permalinks, categorías y plantillas personalizadas, empleando otros lenguajes como markdown, liquid, html, css...

De alguna manera, Jekyll gestiona contenidos, pero no proporciona una plataforma de gestión. Para ello, creamos Sutty, agregando además otras características que mejoran la tecnología.

## ¿Por qué no se conoce mucho esta alternativa de sitios estáticos por sobre "los más populares" dinámicos?

Si bien es verdad que los sitios dinámicos, en especial los sitios en Wordpress, constituyen hoy la mayor parte de las página web en la Internet, desde hace unos años los sitios estáticos están ganando popularidad.
Gestores como Wordpress y Drupal habilitan la edición inmediata de los sitios web, como decíamos al comentar su tecnología, requieren de varios componentes que deben funcionar de manera constante para que el sitio permanezca accesible. Basta que una parte falle para que el sitio se caiga o sufra ataques.

Un sitio estático es generado por cada actualización, independientemente de las visitas, con lo que se ahorran recursos y disminuyen las fallas y las vulnerabilidades.

El gran problema es que esta información técnica quedaba reservada - hasta ahora - a algunos círculos de personas programadoras, fanáticas o entusiastas de las tecnologías y hackers. **Creemos que hay un modelo tecnológico y político en disputa y bregamos porque las tecnologías que consideramos más confiables, seguras y eficientes puedan ser accesibles para diferentes comunidades, en lugar de quedar reservadas para una elite hacker fundamentalmente angloparlante**. ¡Eso es hacktivismo!

## ¿Los sitios estáticos solo pueden ser utilizados por profesionales de la computación?

No. Es cierto que quienes estén más familiarizades con los distintos lenguajes podrían sacarle más provecho, encontrando nuevas formas de personalizar sus sitios. Pero la propuesta de Sutty es acercar una gestora de contenidos para que tener un sitio estático en Jekyll sea más fácil para personas que no tengan o no quieran adquirir los conocimientos técnicos para montarlo. Aunque incluso quienes tengan más experiencia programando, podrán encontrar en Sutty una aliada para generar sitios en menor tiempo y con optimizaciones para hacer su gestión más fácil y amable.

## ¿Cuál es el impacto ambiental de navegar en Internet? ¿Por qué dicen que sus sitios son ecológicos?

Además de sumarnos a diferentes campañas por la preservación de nuestro planeta, desde Sutty queremos contribuir visibilizando que cada cosa que hacemos o usamos en Internet depende de recursos que sacamos del medioambiente.

Por eso nos preocupamos y nos ocupamos de generar sitios livianos, esto es, que ocupen poco espacio y que consuman menos recursos. La tecnología de los sitios estáticos en la que nos basamos desde Sutty, desde el vamos consume menos recursos porque prescinde de la continua generación del sitio a cada visita.

Además, continuamente evaluamos formas de optimizar nuestros servidores para que consuman menos recursos, al tiempo que participamos de la investigación y bregamos por la implementación de alternativas ecológicas en infraestructura.

## En serio, si no tengo mucha idea sobre tecnología: ¿el gestor de contenidos me va a resultar amigable?

Sutty está pensado para ser accesible para todas las personas, sin importar sus conocimientos técnicos en programación. Para ello, probamos nuestro software con diferentes grupos de usuaries activistas. Si bien no nos gusta del todo pensar en las tecnologías como "fáciles" o "simples" u otras metáforas que tienen a subestimar a les usuaries (sobre todo, porque a menudo esconden a le usuarie las complejidades o complicidades ocultas, como software que trackea o vigila comportamientos sin que le usuarie lo sepa), creemos en encontrar maneras diversas de acercar el software que consideramos más justo y confiable a nuestros colectivos y organizaciones.

Si tenés alguna recomendación o comentario para hacernos al respecto ¡estamos deseoses de conocerla!  [**¡Escribinos!**](index.html#contacto)

## ¿Qué pasa si me atacan y tiran abajo mi sitio?

Si el sitio está alojado en tu servidor o un servidor contratado por vos o tu organización, será cuestión de que puedas investigar el ataque y eventualmente darle de alta. Si se encuentra en nuestro servidor, quedará bajo nuestra responsabilidad (podés repasar nuestros T **érminos y Condiciones de servicio** para mayor información).

De todas maneras, Sutty te da la opción de tener siempre una copia de respaldo actualizada (¡y liviana!) de manera que puedas levantar tu sitio rápidamente en otro lado o incluso tener tu sitio funcionando en más de un servidor o ubicación. Asimismo, será sencillo guardar, crear y compartir copias de tu sitio para ser vistas sin conexión a Internet.

Estos métodos, además, son una buena forma de combatir la censura en Internet y asegurarnos que nuestros contenidos lleguen a la mayor cantidad de gente posible.

Estamos trabajando en incorporar otras posibilidades de almacenamiento desde el Panel de Sutty, así como algunas guías de asistencia y notas informativas.

## ¿Hay límite para los sitios que puedo crear?

En Sutty, creemos que cada usuarie o grupo de usuaries puede ser responsable por el uso que le da a su cuenta. Al mismo tiempo, nuestro modelo de sustentabilidad se basa en la solidaridad, por lo que no creemos que ofrecer limitaciones sea algo de nuestra incumbencia. Confiamos en que, si bien nuestros sitios son livianos, darán el uso que consideren justo de acuerdo a sus necesidades, con el cumplimiento de nuestros términos y condiciones y licencias afines. Es decir que no: no hay un límite establecido como política.

## ¿El servicio es gratuito? ¿Me van a comenzar a cobrar de un día para el otro?

No cobramos por los sitios que hagas a través de Sutty con los recursos y las herramientas ya disponibles. Nos guía el principio de la solidaridad entre activistas, colectivos y organizaciones sociales y de la economía solidaria. Sin embargo, para que el proyecto crezca y pueda autosustentarse, necesitamos también de tu **solidaridad** , ya sea a través de donaciones, recomendaciones, contribuciones u otros intercambios.

Otra opción que proponemos para financiar el proyecto, es la posibilidad de contratarnos para algún diseño o sitio específico, o incluso para organizar talleres de capacitación.

[**¡Contactanos con tus propuestas!**](index.html#contacto)

[**¡Seguimos buscando financiamiento!**](https://donaciones.sutty.nl/)
