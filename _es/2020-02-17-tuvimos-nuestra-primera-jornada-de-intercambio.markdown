---
title: "¡Tuvimos nuestra Primera Jornada de Intercambio!"
description: Primera Jornada de Feedback de Sutty. Desarrollo colaborativo-participativo comunitario
author:
- Sutty
image:
  description: 'Participantes de la Jornada reunides, haciendo la actividad; proyector en el fondo, pizarra con cartulinas de la actividad '
  path: public/62wv8ntxrblkrbc2xllixkmmp1w0/photo_2020-02-11_19-44-51 (2).jpg
tags:
- Feedback
- Jornada de Intercambio
- Desarrollo Participativo
- Features
- Trabajo Colaborativo
- Crear un sitio
- Crear un sitio con Sutty
order: 8
layout: post
uuid: c5f8d70c-4563-4934-ae9a-f9cccf4ecf16
liquid: false
usuaries:
- 5
---

Queremos agradecerles a todes quienes se acercaron y al Centro Cultural Tierra Violeta por alojarnos y contarles cómo fue la jornada de intercambio de Sutty. Fue una tarde muy linda, comimos manzanitas y galletitas y tomamos mucho mate. Contamos cómo surgió Sutty y cómo quisiéramos que continúe creciendo, con la participación de nuestres compañeres y usuaries de las comunidades de las que somos parte.

A través de una dinámica lúdica, pusimos en orden de prioridad una lista de ideas que teníamos y otras que aportaron les participantes.

![cartelitos con las features/herramientas a desarrollar](https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbU1EIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d0fe254be5740238ca25e2fcf5ce7f9dfdfef572/photo_2020-02-11_20-27-31.jpg)

![Fuimos ordenando los cartelitos con las tareas](https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbVFEIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--ecdce8e424a892701b8f599ae4c5fb9c27cf6e1e/photo_2020-02-11_19-44-49.jpg)

De aquí salió el plan en el que vamos a estar trabajando, con la ayuda de compas diseñadorxs. Pronto contarán con nuevos diseños disponibles desde el panel de Sutty, organizadas según el objetivo del sitio: para medios comunitarios, campañas de donación, campañas de junta de adhesiones, etc...

Subimos todo a nuestro repositorio en [0xacab](https://0xacab.org/sutty/sutty/-/boards/1392). Pueden entrar, darle una mirada, mantenerse al tanto y colaborar.

Si piensan que Sutty puede aportar para su proyecto o tienen alguna duda acerca de cómo colaborar no duden en [escribirnos](https://sutty.nl/index.html#contacto). ¡Queremos seguir intercambiando!


![Manzanas, stickers en la compu y de fondo, la exposición del CC Tierra violeta](https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbVVEIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--09a0841475337f0270a1cae59424d390ac01b932/photo_2020-02-11_20-27-29.jpg)

![Fruta, agua, agua para el mate y dispositivos para escribir: celu, compus y cartulinas](https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbVlEIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--5d31626f1d83daad22c23be1ca997fc3a42f3504/photo_2020-02-11_20-27-30.jpg)

![Vimos cómo crear un sitio con Sutty](https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbWNEIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--83f3a4f4de34e1e470510abbc9f61e1bd2c077e9/photo_2020-02-11_19-44-49%20(2).jpg)

