---
title: Plantillas para Crear Catálogos de Editoriales Autogestivas
description: Un nuevo template para la economía social y solidaria inspirado en proyectos de edición
author:
- Sutty
image:
  description: ''
categories:
- Plantillas
tags:
- Editoriales Autogestivas
- Economía Social y Solidaria
order: 12
layout: post
uuid: 70f03dd2-c37c-49ea-b2c6-a9296b6650c2
liquid: false
usuaries:
- 5
---

Sutty busca que proyectos colectivos y solidarios puedan acceder a la tecnología web más segura, eficiente y liviana que hay disponible, basada en Software Libre y sus principios éticos, a la hora de crear sus sitios web. Tras una [jornada de intercambio con distintxs colectivxs y diseñadorxs](https://sutty.nl/tuvimos-nuestra-primera-jornada-de-intercambio/), nos propusieron que creáramos plantillas (templates o themes) orientadas directamente a funcionalidades específicas que este tipo de proyectos requieren. Las plantillas, templates o themes son bases de diseño que, en nuestro caso, permiten cubrir necesidades de comunicación y no solo estéticas. De esta manera, les creadorxs de contenido pueden alcanzar sus objetivos más rápidamente.

La plantilla "Editorial Autogestiva" fue inspirada en compañeres de la Feria feminista disidente y autogestiva _La Bastardilla_, quienes, a raíz de la cuarentena, comenzaron a necesitar espacios digitales para mostrar sus publicaciones. Con este template, es muy simple crear un sitio web donde generar un catálogo y asociarle un link de pago que incluye métodos de pago online.

Además, nos enfocamos en lograr que estos catálogos aparezcan primero en las búsquedas. Para eso, le agregamos un vocabulario estructurado de datos llamado schema.org que genera la estructura de metadatos necesaria para que Google los reconozca como objetos en venta. De esta manera, cuando les usuaries busquen adquirir algunos de estos títulos en Internet, tienen más chances de llegar a este sitio web.

Para usarla, basta con [registrarse en nuestro sitio](https://panel.sutty.nl/usuaries/sign_in) (no pedimos datos personales: tan solo una cuenta de correo). Una vez en el panel, podrán crear su sitio y elegir la plantilla de Editorial Autogestiva dentro de las opciones de creación.

Pueden ver algunos sitios creados con esta base de diseño y código:

- [Kinky Vibe](https://kinkyvibe.sutty.nl/)
- [Sube la Marea](https://subelamarea.sutty.nl/)
- [La Disgráfica](https://ladisgrafica.sutty.nl/)
- [Tierra del Sur](https://tierradelsur.sutty.nl/)
