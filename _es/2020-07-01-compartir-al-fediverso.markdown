---
title: Compartir al Fediverso
description: El Fediverso es la red de redes sociales libres donde podemos comunicarnos,
  compartir y organizarnos autónomamente.
author:
- Sutty
image:
  description: Captura de pantalla mostrando el sitio de compartir al fediverso, compartiendo
    este mismo artículo
  path: public/vdz1doaihzdrcr3inqq5l22zvoby/Screenshot_2020-07-01 Compartir en el
    Fediverso.png
permalink: compartir-al-fediverso/
categories:
- Plantillas
- Técnica
tags:
- Plantillas
- Fediverso
order: 15
layout: post
uuid: a171670c-9486-4de0-b31f-0c5dace34b31
liquid: false
usuaries:
- 1
---



## ¿Qué es esto del "Fediverso"?

Una de las características principales del Fediverso es su decentralización, también conocida como federación.  En lugar de crearnos une usuarie en un proveedor único que centraliza, controla, modera, censura (a unes pero no a otros), monetiza y vigila, podemos elegir el nodo o instancia con la comunidad que más nos atraiga, e incluso con un poco de ganas, empezar uno nuevo.

> ¡Nosotres estamos en [Todon.nl](https://todon.nl/@sutty "Perfil de Sutty en el Fediverso")!

Lo importante es que estas instancias no están aisladas entre sí, sino que nos podemos comunicar de una a otra.  Entonces, no es necesario tener usuarie en todas las comunidades que nos interesan para poder participar de ellas.

Otro aspecto de las instancias del Fediverso es que no son espacios totalmente abiertos.  Cada instancia decide con cuáles otras se *federan*, de forma que muchas instancias facistas han sido bloqueadas en gran parte del Fediverso (aunque en otras no).

En resumen:

* Podemos elegir habitar el nodo o instancia de la comunidad que más nos atraiga

* Estas instancias no están aisladas entre sí, sino que se pueden comunicar de una a otra

* Cada instancia tiene criterios políticos sobre con quién se federa

* Muchas instancias fascistas ya han sido bloqueadas del Fediverso

## Cómo se relaciona con Sutty

En Sutty nos comprometemos a apoyar a todos los proyectos que permitan una participación en Internet autónoma y libre de violencias.  En este sentido, entendemos que el Fediverso es un gran aliado de nuestros objetivos.

En estos últimos tiempos estuvimos desarrollando [una plantilla](plantilla-para-campañas-de-adhesiones/ "Plantilla para campañas de adhesiones") con un especial foco en compartir a redes.  Entendemos que muchas personas aun no se han decidido o no conocen qué es el Fediverso y entonces esos botones de compartir suelen apuntar a un conjunto de redes que cuestionamos por su capacidad de vigilar y explotar y que nos mantienen en un [estado constante de exposición a violencias](todes-estamos-muy-ansioses-en-twitter/ "Todes estamos muy ansioses en Twitter").

Nos planteamos agregar un botón de compartir al Fediverso, pero habiendo tantas instancias, ¿cómo podíamos crear un solo botón que permita compartir a la instancia de cada quién?

## ¡Compartir al Fediverso!

Teniendo en cuenta que este podía ser un problema general, desarrollamos un sitio específico que actúa como intermediario entre la página que queremos compartir y la instancia que aloja nuestre usuarie.

> [Compartir al Fediverso](https://fediverse.sutty.nl/es/?u=https://sutty.nl/compartir-al-fediverso/&t=Sutty:%20Compartir%20al%20Fediverso&d=Sutty%20desarroll%C3%B3%20un%20sitio%20que%20nos%20permite%20generar%20botones%20de%20compartir%20al%20Fediverso%20:O)

Al abrir ese vínculo, van a llegar a un sitio que nos muestra lo que estamos a punto de compartir y nos pregunta en qué instancia nos alojamos.  E incluso puede recordarla, si queremos, para no tener que completarla cada vez.

A partir de ahora cualquier sitio (hecho con Sutty o no) puede tener un botón de compartir solo incluyendo una dirección como la de más arriba como vínculo o botón.

## ¿Por qué Mastodon?  ¡El Fediverso es más diverso!

Mastodon es el *software* que funciona detrás de cada instancia.  Pero en el Fediverso hay más diversidad: *Pleroma*, *Pixelfed*, *Diaspora*, *GNU Social*, *Halcyon*...

Solo podemos decir que comprendemos la hegemonía actual de Mastodon y en este sentido [queremos seguir agregando soporte para otras redes](https://0xacab.org/sutty/jekyll/share-to-fediverse-jekyll-theme/-/issues/1 "Discusión y progreso del soporte para otras redes").  Si querés ayudarnos, ¡[compartir al Fediverso es software libre](https://0xacab.org/sutty/jekyll/share-to-fediverse-jekyll-theme)!