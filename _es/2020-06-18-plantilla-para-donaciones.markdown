---
title: Plantilla para Donaciones
description: Una nueva plantilla de Sutty para campañas de donación, apoyo a iniciativas colectivas o particulares y aportes para la economía social y solidaria y redes de apoyo entre pares
author:
- Sutty
image:
  description: ''
categories:
- Plantillas
tags:
- Donaciones
- Economía Social y Solidaria
order: 14
layout: post
uuid: c3665834-5164-4299-ba44-dbf1a7fd4c13
liquid: false
usuaries:
- 5
---

En el contexto del Covid-19, ciertas necesidades comunitarias requieren de atención inmediata.

La pandemia y sus agravantes estructurales generan urgencia en compilar, recuperar y centralizar pedidos de donaciones varios.

Muchos grupos, colectivos, organizaciones, personas y actorxs de la economía social y solidaria precisan apoyo.

Desde Sutty, diseñamos una plantilla específica para viabilizar donaciones con gran facilidad. Esto significa que organizaciones, grupos, colectivos y/o personas independientes que quieran difundir medios de colaboración, donación, aportes solidarios y/o pagos ahora pueden crear sus sitios específicos para ello en nuestra plataforma Sutty.

Con este template podrán:

- Describir las características y motivaciones de los proyectos o pedidos de ayuda.
- Especificar medios alternativos de pago.
- Agregar los botones de pago para realizar operaciones a través de los servicios correspondientes.

Para usarla, basta con [registrarse](https://panel.sutty.nl/usuaries/sign_in) en nuestro sitio (no pedimos datos personales: tan solo una cuenta de correo). Una vez en el panel, pueden crear su sitio y elegir la "Plantilla de Donaciones" para comenzar.

Si tienen interés en que mostremos su sitio junto a otros en una página de Sutty que centraliza iniciativas de apoyo entre pares, pueden escribirnos a través de nuestro [formulario de contacto](index.html#contacto).

Pueden ver este template aplicado en el siguiente link: [https://donaciones.sutty.nl/](https://donaciones.sutty.nl/)
